'use strict';

/**
 * @ngdoc function
 * @name genyzViewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the genyzViewApp
 */
angular.module('genyzViewApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    
  });
