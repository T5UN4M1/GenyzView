'use strict';

/**
 * @ngdoc function
 * @name genyzViewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the genyzViewApp
 */
angular.module('genyzViewApp')
  .controller('RegisterCtrl', function () {
        this.page = 1;
        
        this.togglePage = function(){
            this.page = (this.page === 1) ? 2 : 1;
        };


    
  });
