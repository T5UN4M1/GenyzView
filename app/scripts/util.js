'use strict';


// remplir l'id du select avec les jours dans le mois
function fillDays(id) {
    var htmlContent = "";
    for (var i = 1; i < 32; ++i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $(id).html(htmlContent);
}
// pareil pour les mois
function fillMonths(id) {
    var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        htmlContent = "";
    for (var i = 0; i < 12; ++i) {
        htmlContent += '<option value="' + i + '">' + month[i] + '</option>';
    }
    $(id).html(htmlContent);
}
// remplis un formulaire de selects de birthDay (id automatique) avec les bonnes options
function fillBirthDayForm(baseId) {

    fillDays("#"+baseId+"Day");
    fillMonths("#"+baseId+"Month");

    var today = new Date(),
        todayYear = today.getFullYear(),
        htmlContent = "";
    for (var i = todayYear; i > todayYear - 100; --i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $("#"+baseId+"Year").html(htmlContent);
}
// remplir un formulaire de selects de dates pour le futur (remplir la date d'un projet futur par ex)
function fillFutureDate(baseId) {
    var today = new Date(),
        todayYear = today.getFullYear(),
        htmlContent = "";

    fillDays("#"+baseId+"Day");
    fillMonths("#"+baseId+"Month");

    htmlContent = "";
    for (var i = todayYear; i < todayYear + 30; ++i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $("#"+baseId+"Year").html(htmlContent);
}
// fonction plus élaborée de remplissage de date, par ex 30 , 50 pour les jumps signifie que la date ira d'il y à 30 ans à dans 50 ans (pour 2018 on pourra choisir une date entre 1988 et 2068)
function fillDate(baseId, yearLowJump, yearHighJump) {
    var today = new Date(),
        todayYear = today.getFullYear(),
        htmlContent = "";
    fillDays("#"+baseId+"Day");
    fillMonths("#"+baseId+"Month");

    for (var i = todayYear - yearLowJump; i < todayYear + yearHighJump; ++i) {
        htmlContent += '<option value="' + i + '">' + i + '</option>';
    }
    $("#"+baseId+"Year").html(htmlContent);
}