angular.module('genyzViewApp').component('inputText', {
    templateUrl: 'inputText.html',
    bindings: {
        "id" : "@",
        "label": "@",
        "type": "@",
        "placeholder": "@",
        "value": "@"
    }
  });